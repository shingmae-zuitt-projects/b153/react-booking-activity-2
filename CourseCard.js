import { click } from '@testing-library/user-event/dist/click';
import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';

export default function CourseCard({courseProp}){
    console.log(courseProp)
    const {id, name, description, price, onOffer} = courseProp
    const [count, setCount] = useState(0)
    function enroll(){
        setCount(count + 1);
        if(count === 29){
          document.getElementsByClassName("Button").disabled = true;
          alert("Sorry. Enrollment are now full.");
        }
        
    }
    return (
        <Card>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Decription:</Card.Subtitle>
          <Card.Text>
            {description}
          </Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>{price}</Card.Text>
          <Card.Text>Enroll: {count}</Card.Text>
        <Button class="Button" variant="primary" onClick={enroll}>Enroll</Button>
        </Card.Body>
      </Card>
    )
}
